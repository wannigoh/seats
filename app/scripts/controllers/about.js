'use strict';

/**
 * @ngdoc function
 * @name sourceApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sourceApp
 */
angular.module('sourceApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
