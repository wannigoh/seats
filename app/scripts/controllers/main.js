'use strict';

/**
 * @ngdoc function
 * @name sourceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sourceApp
 */
angular.module('sourceApp')
  .controller('MainCtrl', function ($scope, $timeout, $rootScope) {


    $scope.totalColumns = 10;
    $scope.rows = 'abcdefghijklmnopqrstuvwxyz'.split('');
    $scope.ticketNumber;

    $rootScope.showMessage = false;
    $rootScope.message = '';
    $rootScope.score = 0;
    $rootScope.correct = false;
    $rootScope.bottomRow;




    $scope.setRange = function(num) {
        return new Array(num);
    }

  })
  .directive('scroll', function( $window, $rootScope ){
    return function(scope,element, attrs){
        var bottomRow = $('.col__numbers--bottom');

        angular.element($window).bind('scroll resize', adjust);
        angular.element($window).trigger('resize');

        function adjust() {
          if ( $window.innerWidth <= 736) {
            var offset = ($window.pageYOffset + $window.innerHeight - 33);
            bottomRow.css({ top: offset + 'px'});
          } else {
            bottomRow.removeAttr('style');
          }
        }

    }
  })
  .directive('rabbit', function( $rootScope ){
    return {
      link: function ( scope, elemen, attrs ) {

        var $rabbit = $('.rbt');

        // generate a new rabbit every time one is correctly seated
        scope.$on('seated', function(){
          scope.newTicket();
          scope.showRabbit();
        });

        // create new movie ticket
        scope.newTicket = function() {

          var randomRow = scope.rows[ Math.ceil( Math.random() * scope.rows.length) ];
          var randomColumn = Math.ceil( Math.random() * scope.totalColumns );

          scope.ticketNumber = randomRow + randomColumn;

        };

        // animate the rabbit
        scope.showRabbit = function() {
          TweenMax.to( $rabbit, 1, { y: 0, delay: 0.3, ease: Back.easeInOut });
          scope.$apply();
        }

        // first ticket
        scope.newTicket();

      }
    }
  })
  .directive('seat', function( $rootScope, $timeout ){
    return {
      link: function ( scope, element, attrs ) {


        scope.seatNumber = attrs.seatNumber;
        scope.seated = false;


        scope.seatClick = function(){

          $rootScope.showMessage = true;

          // hide message after a 1 second delay
          $timeout(function(){
            $rootScope.showMessage = false;
          }, 1000);


          // correct seat selected
          if ( scope.seatNumber == scope.ticketNumber ) {

            console.log('correct seat');
            scope.seated = true;
            $rootScope.message = 'Well done!'
            $rootScope.correct = true;
            $rootScope.score++;

            // hide rabbit
            TweenMax.to( $('.rbt'), 1, { y: 240, delay: 0.3, ease: Back.easeInOut, onComplete: function(){
                scope.$emit('seated');
              }
            });

          // wrong seat selected
          } else {
            console.log('wrong seat');
            $rootScope.correct = false;
            $rootScope.message = 'Try again!';
          }
        }


      }
    };
  });

